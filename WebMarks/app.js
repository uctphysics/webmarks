var express = require('express');
var favicon = require('serve-favicon');
var dbConfig = require('./secrets.json');
var http = require('http');
var path = require('path');
var sassMiddleware = require('node-sass-middleware');

var mysql = require('mysql');

var dbPool  = mysql.createPool({
  connectionLimit : 10,
  host: '127.0.0.1',
  user: dbConfig.webmarksUser,
  password: dbConfig.webmarksPassword,
  database: 'webapp'
});

var port = process.env.PORT || 1337;

var app = express();

// all environments
app.set('views', __dirname + '/views');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.set('view options', { pretty: true });
app.use(express.static(__dirname + '/public'));
app.use(favicon(__dirname + '/public/images/favicon.png'));

//sass config
app.use(sassMiddleware({
    /* Options */
    src: path.join(__dirname, 'sass'),
    dest: path.join(__dirname, 'public/css'),
    debug: false,
    outputStyle: 'compressed',
    response: true,
    prefix: '/css'
}));

require('./routes/redirects')(app);
require('./routes/selectStudent')(app, dbPool);
require('./routes/marks')(app, dbPool);
require('./routes/randomNames')(app, dbPool);

    
//app.use(express.static('public'));

//app.get('*', function (req, res) {
//    res.redirect("/");
//});

if (process.env.LISTEN_HOST)
    app.listen(port, process.env.LISTEN_HOST, function () {
        console.log('Listening on port ' + port + ' and address ' + process.env.LISTEN_HOST);
    });
else
    app.listen(port, function () {
        console.log('Listening on port ' + port);
    });
