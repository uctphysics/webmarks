# UCT Physics WebMarks#

UCT Physics WebMarks is a node.js app for serving up-to-date marks to students via a web browser. The site is visible at [http://webapp-phy.uct.ac.za/webmarks](http://webapp-phy.uct.ac.za/webmarks)

### Technologies Used ###

* node.js
* MySQL
* Express server + Jade templating engine
* bootstrap CSS, Highcharts JavaScript library